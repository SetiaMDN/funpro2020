import Lambda

main :: IO ()
main = do
  putStrLn "Welcome to a basic lambda interpreter."
  putStrLn "Syntax: xnx where x is number (0-9) and n is operator"
  putStrLn "Supported operators: + and x or *"
  s <- getLine
  takeIO (take 1 s) (tail (init s)) (drop 2 s)

takeIO x y z = takeIO2 (read x :: Int) y (read z :: Int)

takeIO2 x y z = run_norm x y z
